import React, { Component } from 'react'
import { connect } from 'react-redux'
import "./userForm.css"
import { userFormReducer } from './Redux/reducers/userReducer';
import { type } from '@testing-library/user-event/dist/type';

export class TableFormUser extends Component {

  renderUserList = () => { 
    return this.props.mangUserForm.map((item, index) => {
      return <tr key={index}>
        <td className='vertical-align'>{item.id}</td>
        <td className='vertical-align'>{item.fullName}</td>
        <td className='vertical-align'>{item.phoneNumber}</td>
        <td className='vertical-align'>{item.email}</td>
        <td className='vertical-align'>
          <button onClick={() => {
            this.props.handleRemove(item.id)
          }} className='btn btn-danger mr-2'>Remove</button>
          <button onClick={() => {
            this.props.handleEdit(item)
          }} className='btn btn-success'>Edit</button>
        </td>
      </tr>
    })
   }

  render() {
    return (
      <div className='mt-4'>
        <table className="table">
          <thead className='text-white bg-dark'>
            <th>Mã sinh viên</th>
            <th>Họ và tên</th>
            <th>Số điện thoại</th>
            <th>Email</th>
            <th>Action</th>
          </thead>
          <tbody>
            {this.renderUserList()}
          </tbody>
        </table>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    mangUserForm: state.userFormReducer.userArr,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleRemove: (idUser) => {
      dispatch({
        type: "REMOVE_USER",
        payload: idUser
      })
    },
    handleEdit: (user) => {
      dispatch({
        type: "EDIT_USER",
        payload: user
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TableFormUser)