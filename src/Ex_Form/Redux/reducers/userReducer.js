const initialState = {
    userArr: [{
        id: "24",
        fullName: "Le Hong Nguyen",
        email: "nguyenle241299@gmail.com",
        phoneNumber: "0349671757",
    }],
    userEdited: [{
        id: "24",
        fullName: "Le Hong Nguyen",
        email: "nguyenle241299@gmail.com",
        phoneNumber: "0349671757",
    }],
    searchUser: []
}

export const userFormReducer = (state = initialState, action) => { 
    switch(action.type) {
        case "ADD_USER": {
            let userArrUpdate = [...state.userArr, action.payload]
            state.userArr = userArrUpdate;
            return {...state}
        }

        case "REMOVE_USER": {
            let cloneUserArr = [...state.userArr];
            let index = cloneUserArr.findIndex((item) => {
                return item.id == action.payload
            });
            console.log(index)
            if(index !== -1) {
                cloneUserArr.splice(index,1)
            }
            state.userArr = cloneUserArr;
            return {...state}
        }

        case "EDIT_USER": {
            console.log(action)
            let cloneEditedArr = [...state.userEdited];
            cloneEditedArr.push(action.payload);

            state.userEdited = cloneEditedArr;
            return {...state}
        }
        default:
            return {...state};
    }
 }