import { combineReducers } from "redux";
import { userFormReducer } from './userReducer';

export const rootReducer = combineReducers({userFormReducer})