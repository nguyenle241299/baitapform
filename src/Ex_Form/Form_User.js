import React, { Component } from "react";
import { connect } from "react-redux";
import "./userForm.css";
import { userFormReducer } from "./Redux/reducers/userReducer";

export class Form_User extends Component {
  state = {
    user: {
      id: "",
      fullName: "",
      email: "",
      phoneNumber: "",
    },
    error: {
      id: "",
      fullName: "",
      email: "",
      phoneNumber: "",
    },
  };

  UNSAFE_componentWillReceiveProps = (nextProps) => {
    console.log(nextProps);
    if (nextProps.mangEdited) {
      this.setState({
        user: nextProps.mangEdited,
      });
    }
  };

  handleChangeUser = (e) => {
    let { name, value, type } = e.target;

    let errMessage = "";
    if (value.trim() === "") {
      errMessage = "This field is invalid";
    } else if (type === "email") {
      var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
      if (!pattern.test(value)) {
        errMessage = name + " " + "is invalid";
      }
    } else if (type === "number") {
      var pattern = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
      if (!pattern.test(value)) {
        errMessage = name + " " + "is invalid";
      }
    }

    let cloneUseruser = { ...this.state.user, [name]: value };
    let cloneErr = { ...this.state.error, [name]: errMessage };

    this.setState({
      user: cloneUseruser,
      error: cloneErr,
    });
  };

  handleSubmit = () => {
    // validation
    let { user, error } = this.state;
    let isValid = true;

    for (let key in user) {
      if (user[key] === "") {
        isValid = false;
      }
    }

    for (let key in error) {
      if (error[key] !== "") {
        isValid = false;
      }
    }

    if (isValid == false) {
      alert("Dữ liệu chưa hợp lệ");
      return;
    } else {
      let newUser = { ...this.state.user };
      this.props.handleAddUser(newUser);
      alert("Thành công");
      this.setState({
        user: {
          id: "",
          fullName: "",
          email: "",
          phoneNumber: "",
        },
      });
    }
  };

  render() {
    return (
      <>
        <form className="row">
          <div className="form-group col-6">
            <input
              type="text"
              onChange={this.handleChangeUser}
              value={this.state.user.id}
              className="form-control"
              name="id"
              placeholder="Mã sinh viên"
            />
            <span className="form_user d-block text-left mt-1 text-danger">
              {this.state.error.id}
            </span>
          </div>
          <div className="form-group col-6">
            <input
              type="text"
              onChange={this.handleChangeUser}
              value={this.state.user.fullName}
              className="form-control"
              name="fullName"
              placeholder="Họ và tên"
            />
            <span className="form_user d-block text-left mt-1 text-danger">
              {this.state.error.fullName}
            </span>
          </div>
          <div className="form-group col-6">
            <input
              type="number"
              onChange={this.handleChangeUser}
              value={this.state.user.phoneNumber}
              className="form-control"
              name="phoneNumber"
              placeholder="Số điện thoại"
            />
            <span className="form_user d-block text-left mt-1 text-danger">
              {this.state.error.phoneNumber}
            </span>
          </div>
          <div className="form-group col-6">
            <input
              type="email"
              onChange={this.handleChangeUser}
              value={this.state.user.email}
              className="form-control"
              name="email"
              placeholder="Email"
            />
            <span className="form_user d-block text-left mt-1 text-danger">
              {this.state.error.email}
            </span>
          </div>
        </form>
        <div className="row">
{/* 
          <div className="form-group col-4 margin-bottom-0">
            <input
              type="text"
              value={this.state.user}            
              className="form-control"
              placeholder = "Tìm kiếm"
            />
          </div> */}

          <button onClick={this.handleSubmit} className="btn btn-primary">
            Thêm sinh viên
          </button>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    mangEdited: state.userFormReducer.userEdited
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleAddUser: (user) => {
      dispatch({
        type: "ADD_USER",
        payload: user,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Form_User);
