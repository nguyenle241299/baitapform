import React, { Component } from 'react'
import Form_User from './Form_User'
import TableFormUser from './TableFormUser'

export default class Ex_form extends Component {
  render() {
    return (
      <div className='container py-5'>
        <Form_User/>
        <TableFormUser/>
      </div>
    )
  }
}
