import React, { Component } from 'react'
import './App.css';
import Ex_form from './Ex_Form/Ex_form';

function App() {
  return (
    <div className="App">
      <Ex_form/>
    </div>
  );
}

export default App;
